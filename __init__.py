# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import employee
from . import contract

def register():
    Pool.register(
        contract.StaffContract,
        employee.Employee,
        contract.StaffContractFuthermore,
        contract.ContractsEmployeesStart,
        contract.AddFuthermoreStart,
        contract.FuthermoreContractStart,
        module='staff_co', type_='model')
    Pool.register(
        contract.ContractReturnActive,
        contract.ContractsEmployees,
        contract.AddFuthermore,
        contract.FuthermoreContract,
        contract.PriorNotice,
        module='staff_co', type_='wizard')
    Pool.register(
        contract.ContractsEmployeesReport,
        contract.PriorNoticeReport,
        contract.FinishContractReport,
        contract.ContractCertificationReport,
        contract.LegalContractReport,
        contract.OperationalContractReport,
        contract.FuthermoreContractReport,
        contract.ContractClausesReport,
        contract.ReturnEquipmentsReport,
        employee.BankLetterReport,
        module='staff_co', type_='report')
